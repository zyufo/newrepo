namespace UserReservation.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class URModel : DbContext
    {
        public URModel()
            : base("name=URModel")
        {
        }

        public virtual DbSet<reserve> reserve { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<reserve>()
                .Property(e => e.check_in_date)
                .IsUnicode(false);

            modelBuilder.Entity<reserve>()
                .Property(e => e.check_out_date)
                .IsUnicode(false);

            modelBuilder.Entity<reserve>()
                .Property(e => e.Number_of_Guests)
                .IsUnicode(false);

            modelBuilder.Entity<reserve>()
                .Property(e => e.Number_of_rooms)
                .IsUnicode(false);

            modelBuilder.Entity<reserve>()
                .Property(e => e.credit_card)
                .IsUnicode(false);

            modelBuilder.Entity<reserve>()
                .Property(e => e.name_on_credit_card)
                .IsUnicode(false);

            modelBuilder.Entity<reserve>()
                .Property(e => e.credit_card_number)
                .IsUnicode(false);

            modelBuilder.Entity<reserve>()
                .Property(e => e.Expiration_Date)
                .IsUnicode(false);

            modelBuilder.Entity<reserve>()
                .Property(e => e.Username)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Username)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Lastname)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Firstname)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Street)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Provin)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Coun)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Postal)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Email)
                .IsUnicode(false);
        }
    }
}
