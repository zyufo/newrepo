﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UserReservation.Models;

namespace test.Controllers
{
    public class SignupController : Controller
    {
        private URModel _db = new URModel();
        protected override void HandleUnknownAction(string actionName)

        {

            try
            {

                this.View(actionName).ExecuteResult(this.ControllerContext);

            }
            catch (InvalidOperationException ieox)

            {

                ViewData["error"] = "Unknown Action: \"" + Server.HtmlEncode(actionName) + "\"";

                ViewData["exMessage"] = ieox.Message;

                this.View("Error").ExecuteResult(this.ControllerContext);

            }

        }
        // GET: Signup
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult SignupSuccess([Bind(Exclude = "Id")] User u)
        {
            if (!ModelState.IsValid)
                return View();

            _db.User.Add(u);
            _db.SaveChanges();

            return View();
        }
    }
}